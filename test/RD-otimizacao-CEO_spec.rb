require "json"
require "selenium-webdriver"
require "rspec"


include RSpec::Expectations

describe "RD Otimizacao CEO" do

  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "https://www.rdstation.com.br/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []

  end

  it "test_d_otimizacao_ceo" do

    #login
    @driver.get(@base_url + "/login")
    @driver.find_element(:id, "user_email").clear
    @driver.find_element(:id, "user_email").send_keys "william.jablonski@gmail.com"
    @driver.find_element(:id, "user_password").clear
    @driver.find_element(:id, "user_password").send_keys "w1ll14m"
    @driver.find_element(:name, "commit").click
    expect(@driver.find_element(:css, "span.navbar-account-name").text).to eq "Empresa"
    #

    @driver.get(@base_url + "/dashboard")
    @driver.find_element(:link, "Dashboard").click
    @driver.find_element(:link, "Atrair").click
    @driver.find_element(:xpath, "//*[@id='menu']/div/div[2]/ul[1]/li[2]/ul/li[3]/a").click
    @driver.find_element(:id, "on_page_seo_report_keyword").clear
    @driver.find_element(:id, "on_page_seo_report_keyword").send_keys "outsourcing"
    @driver.find_element(:name, "commit").click
    (@driver.find_element(:xpath, "//div[7]").text).should == "×Login efetuado com sucesso!"
    (@driver.find_element(:css, "#on_page_seo_report_form > h3").text).should == "Resultados"
  end

  after(:each) do
    @driver.quit
    @verification_errors.should == []
  end


end