# encoding: utf-8

require "json"
require "selenium-webdriver"
require "rspec"

include RSpec::Expectations

RSpec.describe 'RD Login' do

  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "https://www.rdstation.com.br"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []
  end

  after(:each) do
    @driver.quit
   # @verification_errors.should == []
  end

  it "test_login_sucess" do
    @driver.get(@base_url + "/login")
    @driver.find_element(:id, "user_email").clear
    @driver.find_element(:id, "user_email").send_keys "william.jablonski@gmail.com"
    @driver.find_element(:id, "user_password").clear
    @driver.find_element(:id, "user_password").send_keys "w1ll14m"
    @driver.find_element(:name, "commit").click
    (@driver.find_element(:xpath, "//div[7]").text).should == "×Login efetuado com sucesso!"
    expect(@driver.find_element(:css, "span.navbar-account-name").text).to eq "Empresa"
  end
end